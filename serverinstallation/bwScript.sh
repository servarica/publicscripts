#!/bin/bash
#title           :bwscript.sh
#description     :This script will control & Update Desired bandwidth
#author          :Mohamed Ali
#date            :08-Jun-2020
#version         :3.0
#usage           :bash bwscript.sh
#==============================================================================

cd /root/
#xe vm-list | grep name | awk '{print $4}'| sed '/Control/d' | grep -v "\[XO" >sname.txt
xl vcpu-list | awk '{print $1}' | sort | uniq | grep -v Name | grep -v Control | grep -v Domain >sname.txt
echo $(date) > bwscriptexec.log


xe vm-list params=name-label,dom-id | grep name- | cut -d':' -f2 | awk '{print $1}' > vname.txt
xe vm-list params=name-label,dom-id | grep dom- | cut -d':' -f2 | awk '{print $1}' > domid.txt
#cat comb.txt | grep -v "\-1" | grep -v Contr > fcomb.txt

paste vname.txt domid.txt > comb.txt

cat comb.txt | grep -v "\-1" | grep -v Contr > fcomb.txt

for i in $(cat sname.txt)
do
  printf "\n\n\n"
 echo "Running for $i"
 for j in {0..2}
 do
        ip a | grep $(grep $i fcomb.txt | awk -v v="$j" '{print "vif"$2"."v}')

        if [ $? -eq 0 ];
                then
                    #Found Matching VIF
                        vif=$(ifconfig 2> /dev/null | grep $(grep $i comb.txt | awk -v v="$j" '{print "vif"$2"."v}')| cut -d':' -f1)
                        echo  -e "$vif Available \t\t\t######";
                        uuid=$(xe vm-list name-label=$i | grep uuid | awk '{print $5}')

                        portSpeed=$(xe vm-param-get param-name=other-config  param-key=xenica_port uuid=$uuid)

                        if [ ! -z "$portSpeed" ]; then
                        #if [ $portSpeed -gt 0 ]; then
                                #IF Custom port speed found
                                burst=$(($portSpeed*200))
                                rate=$(($portSpeed*1000))

                                echo -e "Custom Port Found in mbps : $portSpeed";

                                tc qdisc del dev $vif root
                                tc qdisc add dev $vif root tbf rate $portSpeed"mbit" burst $burst"kbit" latency 10ms
                                ovs-vsctl set interface $vif ingress_policing_rate=0
                                ovs-vsctl set interface $vif ingress_policing_rate=$rate
                                ovs-vsctl set interface $vif ingress_policing_burst=$burst
                                echo -e "$vif executed for $portSpeed Mbps \t\t @@@@@@";

                        else
                                # Default 100 MBPS
                                tc qdisc del dev $vif root
                                tc qdisc add dev $vif root tbf rate 100mbit burst 20000kbit latency 10ms
                                ovs-vsctl set interface $vif ingress_policing_rate=0
                                ovs-vsctl set interface $vif ingress_policing_rate=100000
                                ovs-vsctl set interface $vif ingress_policing_burst=20000
                                echo -e "$vif executed for 100Mbps \t\t******";
                        fi





        else
                        #No Matching VIF's
                        echo  "$j VIF NaN";
        fi
 done
done
