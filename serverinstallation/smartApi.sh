#Author : Mohamed Ali

# Syntax
#./smartApi.sh {cpu,network,disk} {set,remove,read} vmname value(only for set)


shopt -s nocasematch
case "$1" in

    network)#  echo "Reached Network"
        case "$2" in
            set)# echo "Set Case"
                uuid=$(xe vm-list params=uuid name-label=$3 | awk '{print $5}' | head -1)
                xe vm-param-set other-config:xenica_port=$4 uuid=$uuid
                printf '{"server":"%s","action":"%s","vmname":"%s","mbps":"%s"}\n' `hostname` "Set" "$3" "$4"
            ;;
            remove)# echo "Remove Case"
                uuid=$(xe vm-list params=uuid name-label=$3 | awk '{print $5}' | head -1)
                xe vm-param-remove param-name=other-config param-key=xenica_port uuid=$uuid 2> /dev/null
                printf '{"server":"%s","action":"%s","name":"%s"}\n' `hostname` "removed" "$3"
            ;;
            read)# echo "Read Case"
                vm=$3;
                uuid=$(xe vm-list params=uuid name-label=$vm | awk '{print $5}' | head -1)
                vif=$(ifconfig | grep vif | cut -d':' -f1 | grep vif`xe vm-list params=dom-id name-label=$vm --minimal`)  2> /dev/null

                port=$(xe vm-param-get param-name=other-config  param-key=xenica_port uuid=$uuid 2>/dev/null)
                if [ $? -eq 0 ]; then
                    printf '{"server":"%s","action":"%s","vif":"%s","name":"%s","mbps_in_xenica_port":"%s"}\n' `hostname` "read" "$vif" "$vm" "$port"
                else
                    printf '{"server":"%s","action":"%s","vif":"%s","name":"%s","mbps_in_xenica_port":"%s"}\n' `hostname` "read" "$vif" "$vm" "null"
                fi
            ;;
            *) echo "Invalid Parameter $1"
            ;;
        esac
    ;;

    cpu)  echo  "Reached CPU"
        case "$2" in
            set) echo "Set Case"

            ;;
            remove) echo "Remove Case"
            ;;
            read) echo "Read Case"
            ;;
            *) echo "Invalid Parameter $1"
            ;;
        esac
    ;;

    disk)  echo  "Reached Disk"
        case "$2" in
            set)
            ;;
            remove)
            ;;
            read)
            ;;
            *) echo "Invalid Parameter $1"
            ;;
        esac
    ;;

    *) echo "Invalid Parameter $1"
    ;;
esac
