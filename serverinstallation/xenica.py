#!/usr/bin/env python
# -*- coding: utf-8 -*-

import errno
import json
import subprocess
import sys

import XenAPIPlugin

sys.path.append('.')
from xcpngutils import configure_logging, run_command



def bwlimit(session, args):
    if not args.has_key("limit"):
        raise Exception("Key 'limit' missing from arguments")
    if not args.has_key("vif_uuid"):
        raise Exception("Key 'vif_uuid' missing from arguments")
    limit = args['limit']
    limitAsNumber = int(limit.replace("mbit", "000"),10)
    brustAsNumber = limitAsNumber * 2
    vif_uuid = args['vif_uuid']
    vif = session.xenapi.VIF.get_by_uuid(vif_uuid)
    vif_record = session.xenapi.VIF.get_record(vif)
    vif_device = vif_record['device']
    #sys.stdout.write(vif_device)
    vm = vif_record['VM']
    vm_record = session.xenapi.VM.get_record(vm)
    #sys.stdout.write(' '.join(vm_record))
    domain_id = vm_record['domid']
    #sys.stdout.write(domain_id)
    vif_name = 'vif'+domain_id+'.'+vif_device
    #sys.stdout.write(vif_name)
    #sys.stdout.write(' limit:' + str(limitAsNumber))
    #sys.stdout.write(' brustAsNumber:' + str(brustAsNumber))
    result = run_command(['tc', 'qdisc', 'del' 'dev', vif_name, 'root'])
    #sys.stdout.write(json.dumps(result))
    result = run_command(['tc', 'qdisc', 'add', 'dev', vif_name, 'root', 'tbf', 'rate', limit, 'burst', '20000kbit', 'latency', '10ms'])
    #sys.stdout.write(json.dumps(result))
    result = run_command(['ovs-vsctl', 'set', 'interface',  vif_name, 'ingress_policing_rate=0'])
    #sys.stdout.write(json.dumps(result))
    result = run_command(['ovs-vsctl', 'set', 'interface',  vif_name, 'ingress_policing_rate='+str(limitAsNumber)])
    #sys.stdout.write(json.dumps(result))
    result = run_command(['ovs-vsctl', 'set', 'interface',  vif_name, 'ingress_policing_burst='+str(brustAsNumber)])
    #sys.stdout.write(json.dumps(result))


    if result['exit']==0:
        return "success"
    else:
        return result['stderr']

def cpulimit(session, args):
    if not args.has_key("limit"):
        raise Exception("Key 'limit' missing from arguments")
    if not args.has_key("vm_uuid"):
        raise Exception("Key 'vm_uuid' missing from arguments")
    limit = args['limit']
    limitAsNumber = int(limit,10)
    vm_uuid = args['vm_uuid']
    vm = session.xenapi.VM.get_by_uuid(vm_uuid)
    vm_record = session.xenapi.VM.get_record(vm)
    #sys.stdout.write(' '.join(vm_record))
    domain_id = vm_record['domid']
    cpu_count = vm_record['VCPUs_max']
    limit_normalized = limitAsNumber * int(cpu_count,10)
    #sys.stdout.write(" domainid:" + str(domain_id))
    #sys.stdout.write(" cpu_count:" + str(cpu_count))
    #sys.stdout.write(" limit:" + str(limit))
    #sys.stdout.write(" limitAsNumber:" + str(limitAsNumber))
    #sys.stdout.write(" limit_normalized:" + str(limit_normalized))
    #xl sched-credit -d [domain] -c [cap]
    result = run_command(['xl', 'sched-credit', '-d', domain_id, '-c', str(limit_normalized)])
    #sys.stdout.write(json.dumps(result))
    if result['exit']==0:
        return "success"
    else:
        return result['stderr']

_LOGGER = configure_logging('pyperthreading')
if __name__ == "__main__":
    XenAPIPlugin.dispatch({
        'bwlimit': bwlimit,
        'cpulimit': cpulimit,
    })

